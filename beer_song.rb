#
# Написать программу, которая выводит на экран песню "Ninety-nine Bottles of Beer on the Wall"
# с заданным колличиством куплетов
#
# Ninety-nine bottles of beer on the wall,
# Ninety-nine bottles of beer,
# Take one down, pass it around,
# Ninety-eight bottles of beer on the wall.
# ...
# One bottle of beer on the wall,
# One bottle of beer,
# Take one down, pass it around,
# Zero bottles of beer on the wall.
#


class BeerSong

  attr_accessor 'number'

  def initialize (number)
    number = 0 if number < 0
    number = 99 if number > 99
    @number = number
  end

  # преобразоване числа в строку
  def get_char number=self.number
    str_number = ""
    char_arr = ["zero", "one", "two", "three", "four", "five", "six", "seven",\
      "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", \
      "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
    an_arr =  ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

    if number < 20
      str_number = char_arr[number]
    elsif number % 10 == 0
      str_number = an_arr[(number / 10) - 2]
    else
      str_number = an_arr[(number / 10) - 2] + '-' + char_arr[number % 10]
    end
    str_number.capitalize!
  end

  # вывод одного куплета
  def print_stanza
    s1 = "s"
    s2 = "s"
    s1 = "" if self.number == 1
    s2 = "" if self.number == 2
    puts "#{get_char} bottle#{s1} of beer on the wall,"
    puts "#{get_char} bottle#{s1} of beer,"
    puts "Take one down, pass it around,"
    puts "#{get_char (self.number-1)} bottle#{s2} of beer on the wall."
  end

# вывод всей песни
  def print_song
    (self.number).times do
      print_stanza
      self.number -= 1
    end
  end

end
